# Common Lib
- The project by is used by GitLab CI/CD Crash Course (https://www.youtube.com/playlist?list=PLigGXNza3t_y9pwBvOzOvIqiq0Gk6wWrd)    
- The  Common Lib project has a sub project common-entities. common-entities only include DTO classes. The classes can be used by other project.   
- The build process detail is on video [How to Publish a Java Project to GitLab Package Registry?] (https://youtu.be/6y7vuNHoQC0)  


