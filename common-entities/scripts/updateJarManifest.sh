#!/bin/bash
#  Original Commands
#  - sed -i 's/#Number#/'$CI_PIPELINE_ID'/g' common-entities/gradle.properties
#  - cat common-entities/gradle.properties
# Update Jar manifest based on  ENV parameters
set -e
echo "Update Jar Manifest Start"
#pwd
#ls -al
sed -i 's/#Number#/'$CI_PIPELINE_ID'/g' common-entities/gradle.properties
cat -n common-entities/gradle.properties
echo -e "\nUpdate Jar Manifest End"
